    #!/bin/tcsh 			  
    				  # first line specifies shell
    #BSUB -J cpu2006_lbm 		  #name the job "jobname"
    #BSUB -o out.lbm   		  #output->   out.o&ltjobID>
    #BSUB -e err.lbm   		  #error -> error.o&ltjobID>
    #BSUB -n 1 -W 12:00                 #4 CPUs and 1hr+30min
    #BSUB -q serial                   #Use normal queue.
    set echo                          #Echo all commands.
    cd $LS_SUBCWD                     #cd to directory of submission
    serialrun time $WORK/aashish/pin-2.0-7259-gcc.3.4-ia32-linux/Bin/pin -t $WORK/aashish/pin-2.0-7259-gcc.3.4-ia32-linux/ManualExamples/proccount -- $WORK/aashish/benchmarks/470.lbm/lbm_base.i386 3000 $WORK/aashish/benchmarks/470.lbm/reference.dat 0 0 $WORK/aashish/benchmarks/470.lbm/100_100_130_ldc.of 
                       #use ibrun for "pam -g 1 mvapich_wrapper"
                                      #CPUs are specified above in -n option.


