#!/usr/bin/python

# Runs several instances of sim-outorder

import sys, os, subprocess
from optparse import OptionParser
from power_manager import power_state, core_state, manager

# SimpleScalar command (does not include benchmark program and args)
#SIM_CALL = [ "./sim-outorder", "-max:inst", "25000000" ]
SIM_CALL = [ "./sim-outorder", "-max:inst", "250000000" ]

# location of benchmark program directory relative to CWD
PROG_DIR = "../spec2006/"

# get the relative path to the benchmark directory
def prog_path(path):
    return os.path.join(PROG_DIR, path)

"""
unused programs

          [ prog_path("403.gcc/cpu2006_403.gcc_kehoste_Alpha_gcc411_O2"),
            prog_path("403.gcc/scilab.i"),
            "-o",
            prog_path("403.gcc/scilab.o") ],


          [ prog_path("470.lbm/cpu2006_470.lbm_kehoste_Alpha_gcc411_O2"),
            prog_path("470.lbm/reference.dat"),
            "0",
            "0",
            prog_path("470.lbm/100_100_130_ldc.of") ],

"""

# programs to run
PROGS = [
          # core 0
          [ "-fastfwd", "1528000000",
            prog_path("401.bzip2/cpu2006_401.bzip2_kehoste_Alpha_gcc411_O2"),
            prog_path("401.bzip2/input.combined"),
            "200" ],

          # core 1
          [ "-fastfwd", "9974000000",
            prog_path("462.libquantum/cpu2006_462.libquantum_kehoste_Alpha_gcc411_O2"),
            "1397",
            "8" ],

          # core 2
          [ "-fastfwd", "13068000000",
            prog_path("436.cactusADM/cpu2006_436.cactusADM_kehoste_Alpha_gcc411_O2"),
            prog_path("436.cactusADM/benchADM.par") ],

          # core 3
          [ "-fastfwd", "1807000000",
            prog_path("450.soplex/cpu2006_450.soplex_kehoste_Alpha_gcc411_O2"),
            "-m3500",
            prog_path("450.soplex/ref.mps") ]

        ]

num_progs = len(PROGS)

# parse command line arguments
opt_parser = OptionParser()

opt_parser.add_option("-l", "--logfile", action="store", type="string",
                      dest="logfile", default="logs/run_sim.log",
                      help="write output log file to LOGFILE")
opt_parser.add_option("-p", "--policy", action="store", type="int",
                      dest="policy", default=None,
                      help="power management policy [default=None]")
opt_parser.add_option("-b", "--budget", action="store", type="float",
                      dest="budget", default=88,
                      help="power budget in watts")
opt_parser.add_option("-t", "--threshold", action="store", type="float",
                      dest="threshold", default=0.5,
                      help="memory bound threshold [0.0, 1.0]")

(options, args) = opt_parser.parse_args()

print "Options:"
print "  Log file :", options.logfile
print "  Policy   :", options.policy
print "  Budget   :", options.budget
print "  Threshold:", options.threshold


# global vars
processes = list()     # information on simulator processes

log_file = open(options.logfile, "w")

#policy
#RUU_full_percent = 0
#RUU_stall_percent = 1
#L2_miss_per_cycle = 2
#load_store_percent = 3
#ipc = 4
#bips = 5
#no management = None
policy = options.policy
budget = options.budget         # in watts
threshold = options.threshold   # membound crossover(point needs to be tuned)



# check if simulator processes are still running
# returns True if all are still running; otherwise returns False
def still_alive ():
    for i in range(len(processes)):
        if processes[i]["proc"].poll() is not None:
            print "Simulator PID", processes[i]["proc"].pid, "has finished!"
            return False
    return True

# kills all simulator processes
# sends "-1" for next interval count
def kill_all ():
    for i in range(len(processes)):
        stdin = processes[i]["stdin"]
        try:
            stdin.write("-1\n")
            stdin.flush()
            stdin.close()
            print "Sending terminate to PID", processes[i]["proc"].pid
        except IOError:
            pass   # no problem; pipe could be closed already

# get and update stats for one process given name of stats file
# returns True if new stats file found; otherwise returns False
def get_stats (proc):
    if not os.path.isfile(proc["statfile"]):
        return False

    f = open(proc["statfile"])
    try:
        # read all lines into a list
        new_stats = f.readlines()

        # must have read at least two lines, otherwise not valid
        if (len(new_stats) < 2):
            return False

        # file is valid if first line is same as last line (cycle count),
        # AND cycle count is different from previous cycle count
        first_line = long(new_stats[0])
        last_line = long(new_stats[-1])

        if proc["cycles"] != first_line and first_line == last_line:
            proc["cycles"] = first_line
            proc["stats"] = new_stats
            return True
        else:
            return False
    finally:
        f.close()

def gen_budget(cur_interval, target_budget):
    # Generate current cumulative power consumption
    cur_power = 0.0;
    cur_interval = cur_interval + 1;
    for i in range(num_progs):
        cur_power = cur_power + float(processes[i]["stats"][8]);
    
    # Generate new budget based on the target
    target_energy = target_budget*(cur_interval+1);
    cur_energy = cur_power*cur_interval;
    new_budget = (target_energy - cur_energy);
    if(new_budget < 0):
        new_budget = 0
    print "TARGET_BUDGET = ", target_budget, " CURRENT_POWER = ", cur_power, " NEW_BUDGET = ", new_budget;
    return new_budget;

# runs the power manager and pipes cycle counts to each simulator process
def run_pwr_mgr (cur_interval, manager, policy):
    print "Running power manager"
    # get previous P-state
    prev_pstate = list()
    for i in range(len(processes)):
        prev_pstate.append(manager.get_pstate(i))
        print "  Core", i, "stats:", processes[i]["stats"]

    # run power manager
    if policy is not None:
        for i in range(num_progs):
            manager.update_power(i, float(processes[i]["stats"][1]))
            manager.set_membound(i, float(processes[i]["stats"][policy+2]), float(processes[i]["stats"][policyAmdahl+2]))
        #gen_budget(cur_interval, budget)
        manager.update_budget(gen_budget(cur_interval, budget))
        manager.evaluate()

    # send power state to SimpleScalar
    for i in range(len(processes)):
        if policy is None:
            pstate = "3"    # default state w/o power manager
        else:
            pstate = str(manager.get_pstate(i))
        processes[i]["stdin"].write(pstate + "\n")
        #processes[i]["stdin"].write("1500000\n")
        print "  Core", i, "runs in pstate", pstate, ", got to ", \
            processes[i]["cycles"], "so far"
        log_file.write(str(prev_pstate[i]) + "\t")
        if policy is not None:
            log_file.write(processes[i]["stats"][policy+2].strip(" \n") + "\t")
        log_file.write(processes[i]["stats"][1].strip(" \n") + "\t" +
            processes[i]["stats"][-2].strip(" \n") + " |\t")
    log_file.write("\n")


num_cores = num_progs
num_pstates = 4
interval = 500 #microseconds
high_threshold = 0.50 #membound crossover(point needs to be tuned)
starting_pstate = 2
pstates = [power_state(3000*(0.75), 1.5*(0.75)), power_state(3000*(0.85), 1.5*(0.85)), power_state(3000*(0.95), 1.5*(0.95)), power_state(3000, 1.5)]
policyAmdahl = 1
max_bips = (policy == 5)
use_amdahl = False
use_edp = False
assign_scratch = False        # assign power states from 0
power_manager = manager(num_cores, num_pstates, budget, interval, threshold, high_threshold, starting_pstate, pstates, max_bips, use_amdahl, use_edp, assign_scratch)


#
# main
#
# clean old files
ret = os.system("rm core*")

if ret is not 0:
    print "WARNING could not remove core* files"

# start running simulation
for i, bench in enumerate(PROGS):
    # create output files for each core's stdout
    outfile = open("core" + str(i) + ".output", 'w')
    # construct the command:
    # SimpleScalar command + core ID parameter + benchmark command
    cmd = SIM_CALL + [ "-coreid", str(i) ] + bench
    print "Executing", cmd
    print "  Outputting to", outfile.name
    p = subprocess.Popen(cmd, stdout=outfile, stderr=subprocess.STDOUT,
        stdin=subprocess.PIPE)
    print "  Started. PID =", p.pid
    processes.append( { "proc"     : p,
                        "stdin"    : p.stdin,
                        "stdout"   : outfile,
                        "statfile" : "core" + str(i) + ".stats",
                        "cycles"   : 0L,
                        "stats"    : list() } )

# check to make sure all programs have been spawned
if num_progs is not len(processes):
    print "ERROR! Did not spawn all processes! Expected", num_progs, \
        ", spawned", len(processes)

# barrier sync variable
finished_flag = 0
cur_interval = 0

# main loop with power manager
while True:
    # check simulators are still alive
    if not still_alive():
        print "At least one simulator process has died... quitting"
        kill_all()
        break

    # wait for simulators to reach next interval
    for proc in processes:
        # check cycle count: if different, increment barrier
        if get_stats(proc):
            finished_flag = finished_flag + 1
    
        # all processes have reported in: run power manager and reset barrier
        if finished_flag == num_progs:
            cur_interval = cur_interval + 1
            run_pwr_mgr(cur_interval, power_manager, policy)
            finished_flag = 0
            break


# simulation finished: close the output files
log_file.close()
for proc in processes:
    proc["stdout"].close()


