#!/bin/sh

for budget in "79" "88" "96"
do
    dir="logs/maxBips_b"${budget}
    log=$dir"/run_sim.log"
    echo "Saving to $dir"
    mkdir $dir
    python run_sim.py -l $log -p 5 -b $budget
    sleep 10
    mv core* $dir
done

