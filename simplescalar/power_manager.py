class power_state(object):
    def __init__(self, f, v):
        self.freq = f #MHz
        self.volt = v

class core_state(object):
    def __init__(self, num_pstates, starting_state):
        self.pstate_index = starting_state   #core's current power state
        self.mem_bound = 0                   #estimated memory bound metric
        self.actual_power = 0                #measured power
        self.estimated_power = []            #projected power in each potential pstate based on actual power and fv scaling
        self.actual_perf = 0                 #used for MAXBIPS
        self.estimated_performance = []      #used for MAXBIPS
        for i in range(num_pstates):
            self.estimated_power.append(0.0)
        
class manager(object):
    def __init__(self, num_cores, num_pstates, budget, interval, threshold, high_threshold, starting_pstate, pstates, maxbips, use_amdahl, use_edp, assign_scratch):
        self.power_budget = budget
        self.available_pstates = pstates
        self.run_interval = interval          #length of 1 run interval in microseconds
        self.mb_thresh = threshold            #mem bound threshold
        self.high_mb_thresh = high_threshold            #mem bound threshold
        self.sorted_list = []                 #used to sort cores acording to memboundedness
 
        for i in range(num_cores):
            self.sorted_list.append(i)

        self.cores = []
        for i in range(num_cores):
            self.cores.append(core_state(num_pstates, starting_pstate))
        #policy re-evaluates from scatch i.e. previous pstates are not used as a starting point is search
        self.assign_scratch = assign_scratch
        #only need to estimate performance if MAXBIPS used
        self.max_bips = maxbips
        self.use_amdahl = use_amdahl   # To run max_eff, both max_bips and max_eff need to be 1
        self.use_edp = use_edp
        if(maxbips):
            for i in range(num_cores):
                for j in range(len(self.available_pstates)):
                    self.cores[i].estimated_performance.append(0.0)

    def update_power(self, core, power):
        self.cores[core].actual_power = power

    def sort_mem_bound(self):
        for i in range(len(self.cores)):
            for j in reversed(range(i+1, len(self.cores))):
                if self.cores[self.sorted_list[j]].mem_bound > self.cores[self.sorted_list[j-1]].mem_bound:
                    temp = self.sorted_list[j]
                    self.sorted_list[j] = self.sorted_list[j-1]
                    self.sorted_list[j-1] = temp

    def amdahls_law(self, cur_perf, cur_freq, new_freq, mem_bound):
        speedup = 1/(mem_bound + (1-mem_bound)/(new_freq/cur_freq));
        #print "Speedup = ", speedup
        return(cur_perf * speedup)

    def estimate_power(self):
        #fill in the projected power consumption for each core in each potential power state
        for i in range(len(self.cores)):
            #calcualte the normalized power
            actual_power_norm = self.cores[i].actual_power
            freq = self.available_pstates[self.cores[i].pstate_index].freq
            volt = self.available_pstates[self.cores[i].pstate_index].volt
            actual_power_norm = actual_power_norm/(freq*volt*volt)
            #fill in the core's table with projected power change
            for j in range(len(self.available_pstates)):
                freq = self.available_pstates[j].freq
                volt = self.available_pstates[j].volt
                estimated_power = actual_power_norm*freq*volt*volt
                #fill in an entry in the estimate table with the estimated  power 
                self.cores[i].estimated_power[j] = estimated_power
                #if max bips is policy, use freq to estimate future performance
                if(self.max_bips):
                    actual_freq = self.available_pstates[self.cores[i].pstate_index].freq
                    if(self.use_amdahl):
                        self.cores[i].estimated_performance[j] = self.amdahls_law(self.cores[i].actual_perf, actual_freq, freq, self.cores[i].mem_bound)
                    else:
                        self.cores[i].estimated_performance[j] = (self.cores[i].actual_perf/actual_freq) * freq 
            print self.cores[i].estimated_power
            

    def set_membound(self, core, mb, mb2):
        #if using MAXBIPS incoming mb is performace otherwise it is the memorybound metric
        if(self.max_bips):
            self.cores[core].actual_perf = mb
        else:
            self.cores[core].mem_bound = mb
        
        # MAXEFF piggybacks off MAXBIPS, so the first mb metric is performance, the second is memorybound
        if(self.use_amdahl):
            self.cores[core].mem_bound = mb2


    def is_membound(self, core):
        if(self.cores[core].mem_bound > self.mb_thresh):
            return(True)
        else:
            return(False)
    def is_very_membound(self, core):
        if(self.cores[core].mem_bound > self.high_mb_thresh):
            return(True)
        else:
            return(False)

    def assign_power(self, additional_power):
        #set the next power states assuming that we can allocate additional power
        for i in reversed(range(len(self.cores))):
            core_num = self.sorted_list[i]
            if( not self.is_membound(core_num)): #check if core could make use of extra power
                current_pstate = self.cores[core_num].pstate_index
                if(current_pstate != len(self.available_pstates) -1):
                    #There is room to raise this core's pstate
                    current_power = self.cores[core_num].estimated_power[current_pstate]
                    new_power = self.cores[core_num].estimated_power[current_pstate + 1]
                    #printf("new power: %f current_power %f additional_power %f\n", new_power, current_power, additional_power);
                    if((new_power - current_power) <= additional_power):
                        #raising this core's power still fits in the budget
                        self.cores[core_num].pstate_index = self.cores[core_num].pstate_index + 1
                        additional_power = additional_power - (new_power - current_power)
            else:
                #no more cores can make use of extra power
                break

    def reclaim_power(self, reclaim_power):
        #set the next power states assuming that we must reclaim some power
        #Step 1: Search the memory bound cores and drop power when possible
        #split the search space into mem bound and non-mem bound cores
        for i in range(len(self.cores)):
           if(not self.is_membound(self.sorted_list[i])):
               break
        #iterate over the mem bound cores until all are at pstate 0 or budget met
        first_non_membound_core = i
        done_search = False
        while (reclaim_power > 0) and not done_search:
            done_search = True
            for i in range(first_non_membound_core):
                core_num = self.sorted_list[i]
                current_pstate = self.cores[core_num].pstate_index
                if((current_pstate != 0) and (reclaim_power > 0)):
                      #There is room to lower this core's pstate
                      current_power = self.cores[core_num].estimated_power[current_pstate]
                      new_power = self.cores[core_num].estimated_power[current_pstate - 1]
                      self.cores[core_num].pstate_index = self.cores[core_num].pstate_index - 1
                      reclaim_power = reclaim_power - (current_power - new_power)
                      done_search = False 
         
        #printf("slack after search 1 = %f\n", reclaim_power);
        #check if there is any need to drop power for non-membound cores
        done_search = False
        while (reclaim_power > 0) and not done_search:
            done_search = True
            for i in range(first_non_membound_core, len(self.cores)):
                core_num = self.sorted_list[i]
                current_pstate = self.cores[core_num].pstate_index
                if((current_pstate != 0) and (reclaim_power > 0)):
                      #There is room to lower this core's pstate
                      current_power = self.cores[core_num].estimated_power[current_pstate]
                      new_power = self.cores[core_num].estimated_power[current_pstate - 1]
                      self.cores[core_num].pstate_index = self.cores[core_num].pstate_index - 1
                      reclaim_power = reclaim_power - (current_power - new_power)
                      done_search = False

    def reclaim_aggresive(self):
        additional_slack = 0.0
        for i in range(len(self.cores)):
            if self.is_very_membound(i):
               additional_slack = additional_slack + self.cores[i].estimated_power[self.cores[i].pstate_index] - self.cores[i].estimated_power[0]
               self.cores[i].pstate_index = 0
        return additional_slack

    def update_budget(self, new_budget):
        self.power_budget = new_budget

    def max_bips_search_inc(self, max_state, search_state):
        index = 0
        max_index = len(search_state) - 1
        rollover = False
        while index <= max_index: 
            if(search_state[index] == max_state):
                search_state[index] = 0
                index = index + 1
            else:
                search_state[index] = search_state[index] + 1
                break     

            if(index > max_index):
                rollover = True
                break
        return rollover

    def assign_power_scratch(self):
        #set the next power states assuming that we must reclaim some power
        #Step 1: Search the memory bound cores and drop power when possible
        #split the search space into mem bound and non-mem bound cores
        power = 0.0
        for i in range(len(self.cores)):
            self.cores[i].pstate_index = 0
            power = power + self.cores[i].estimated_power[0]
        
        for i in reversed(range(len(self.cores))):
            sorted_index = self.sorted_list[i]
            if(not self.is_very_membound(sorted_index)):
               done_search = False
               while (power < self.power_budget) and not done_search:
                   done_search = True
                   current_pstate = self.cores[sorted_index].pstate_index
                   if(current_pstate < (len(self.available_pstates) - 1)):
                         #There is room to lower this core's pstate
                         current_power = self.cores[sorted_index].estimated_power[current_pstate]
                         new_power = self.cores[sorted_index].estimated_power[current_pstate + 1]
                         self.cores[sorted_index].pstate_index = self.cores[sorted_index].pstate_index + 1
                         power = power + (new_power - current_power)
                         done_search = False
     
    def edp(self, power, bips):
        return(bips/power)

    def ed2p(self, power, bips):
        #print "BIPS: ", bips, " power: ", power, "ED2P: ", bips*bips/power
        return(bips*bips/power)

    def ed3p(self, power,bips):
        return(bips*bips*bips/power)

    def perf(self, power, bips):
        return bips;

    def edn23p(self, power, bips):
        return(bips/(power**(2/3)))

    def edn34p(self, power, bips):
        return(bips/(power**(3/4)))

    def edn4p(self, power, bips):
        return(bips/(power**(1/4)))

    def maxbips_search(self):
        max_performance = 0.0
        core_pstates = [] #track search state
        for i in range(len(self.cores)):
            core_pstates.append(0)
        done = False

        # If cannot find anything that works, use P0 for all cores
        for i in range(len(self.cores)):
            self.cores[i].pstate_index = 0

        while not done:
            power = 0.0
            performance = 0.0
            #print "Testing c0 = ", core_pstates[0], " c1 = ",  core_pstates[1], " c2 = ",  core_pstates[2], " c3 = ",  core_pstates[3]
            for i in range(len(self.cores)):
                power = power + self.cores[i].estimated_power[core_pstates[i]]
                if(self.use_edp):
                    performance = performance + self.edp(self.cores[i].estimated_power[core_pstates[i]], self.cores[i].estimated_performance[core_pstates[i]])
                else:
                    performance = performance + self.cores[i].estimated_performance[core_pstates[i]]
            if(power <= self.power_budget):
                if(performance > max_performance):
                    for i in range(len(self.cores)):
                        self.cores[i].pstate_index = core_pstates[i]
                        max_performance = performance
            if(self.max_bips_search_inc(len(self.available_pstates)-1, core_pstates)):
                done = True
                   
    def evaluate(self):
        #find next pstate for each core
        total_power = 0.0
        #find total actual power consumed in the previous interval
        for i in range(len(self.cores)):
            total_power = total_power + self.cores[i].actual_power
        slack = self.power_budget - total_power #negative number means power must be reclaimed
                                                    #positive number means power can be allocated
        #printf("Total power: %f\n", total_power);
        #printf("slack: %f\n", slack);
        self.sort_mem_bound()
        print self.sorted_list 
        self.estimate_power()

        if(self.max_bips):
            for i in range(len(self.cores)):
                print self.cores[i].estimated_performance
            self.maxbips_search()
        elif(self.assign_scratch):
           self.assign_power_scratch()
        else:
           slack = slack + self.reclaim_aggresive() 
           if(slack < 0):
               #printf("reclaming...\n");
               self.reclaim_power(slack*-1)
           else:
               #printf("allocating...\n");
               self.assign_power(slack)

    def get_run_cycles(self, core):
        #figure out how many cycles the next interval will execute
        freq = self.available_pstates[self.cores[core].pstate_index].freq
        cycles = freq * (self.run_interval) #(MHz)*(microseconds) = cycles
        return cycles

    def get_pstate(self, core):
        #figure out how many cycles the next interval will execute
        return self.cores[core].pstate_index

    def get_freq(self, core):
        return(self.available_pstates[self.cores[core].pstate_index].freq)
