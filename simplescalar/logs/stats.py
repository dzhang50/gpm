import os, sys

dir = sys.argv[1]

avg_pwr = list()
num_insts = list()
intervals = 0

for i in range(4):
    filename = os.path.join(dir, "core" + str(i) + ".output")
    f = open(filename)
    try:
        for line in f:
            if line.startswith("avg_total_power_cycle_cc3"):
                split = line.split()
                avg_pwr.append(float(split[1]))
            elif line.startswith("sim_num_insn"):
                split = line.split()
                num_insts.append(float(split[1]))
    finally:
        f.close()

filename = os.path.join(dir, "run_sim.log")
f = open(filename)
try:
    for line in f:
        intervals = intervals + 1
finally:
    f.close()

total_power = 0
total_insts = 0

for i in range(4):
    total_power = total_power + avg_pwr[i]
    total_insts = total_insts + num_insts[i]

print "Total power:", total_power
print "Total insts:", total_insts
print "Time intervals:", intervals
print "Insts/interval:", total_insts / intervals


