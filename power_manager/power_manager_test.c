#include "power_manager.h"
int main(){
    int i;
    manager_state manager;
    power_state pstates[AVAILABLE_PSTATES];
    for(i = 0; i < AVAILABLE_PSTATES; i++){
       pstates[i].freq = 200 + 50*i;
       pstates[i].volt = 3+0.5*i;
    }
    manager_init(&manager, (NUM_CORES*100), 500, 2, pstates);

    for(i = 0; i < NUM_CORES; i++){
        manager_update_power(&manager, i, 150);
    }

    manager_print(&manager);
    
    set_membound(&manager, 0, 200);
    set_membound(&manager, 1, 0);
    set_membound(&manager, 2, 50);
    set_membound(&manager, 3, 150);

    manager_evaluate(&manager);
    manager_print(&manager);

    //----------INTERVAL 2-----------------
    manager_update_power(&manager, 0, 50);
    manager_update_power(&manager, 1, 100);
    manager_update_power(&manager, 2, 50);
    manager_update_power(&manager, 3, 50);
    manager_evaluate(&manager);
    manager_print(&manager);
    //----------INTERVAL 3-----------------
    set_membound(&manager, 0, 150);
    set_membound(&manager, 1, 200);
    set_membound(&manager, 2, 175);
    set_membound(&manager, 3, 150);
    manager_update_power(&manager, 0, 50);
    manager_update_power(&manager, 1, 100);
    manager_update_power(&manager, 2, 50);
    manager_update_power(&manager, 3, 50);
    manager_evaluate(&manager);
    manager_print(&manager);
    //----------INTERVAL 4-----------------
    set_membound(&manager, 0, 15);
    set_membound(&manager, 1, 20);
    set_membound(&manager, 2, 17);
    set_membound(&manager, 3, 15);
    manager_update_power(&manager, 0, 50);
    manager_update_power(&manager, 1, 100);
    manager_update_power(&manager, 2, 50);
    manager_update_power(&manager, 3, 50);
    manager_evaluate(&manager);
    manager_print(&manager);
    //----------INTERVAL 5-----------------
    set_membound(&manager, 0, 50);
    set_membound(&manager, 1, 30);
    set_membound(&manager, 2, 10);
    set_membound(&manager, 3, 0);
    manager_update_power(&manager, 0, 50);
    manager_update_power(&manager, 1, 50);
    manager_update_power(&manager, 2, 50);
    manager_update_power(&manager, 3, 50);
    manager_evaluate(&manager);
    manager_print(&manager);
    //----------INTERVAL 5-----------------
    set_membound(&manager, 0, 50);
    set_membound(&manager, 1, 30);
    set_membound(&manager, 2, 10);
    set_membound(&manager, 3, 0);
    manager_update_power(&manager, 0, 78);
    manager_update_power(&manager, 1, 100);
    manager_update_power(&manager, 2, 100);
    manager_update_power(&manager, 3, 70);
    manager_evaluate(&manager);
    manager_print(&manager);
}
