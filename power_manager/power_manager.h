#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define NUM_CORES 4
#define AVAILABLE_PSTATES 4
#define MEMBOUND_THRESH 100

typedef struct {
    int freq; //MHz
    float volt; 
} power_state;

typedef struct{
    int pstate_index;                        //core's current power state
    int mem_bound;                           //estimated memory bound metric
    float actual_power;                      //measured power
    float estimated_power[AVAILABLE_PSTATES];//projected power in each potential pstate based on actual power and fv scaling
} core_state;

typedef struct{
    core_state cores[NUM_CORES];
    float power_budget;
    power_state available_pstates[AVAILABLE_PSTATES];
    int sorted_list[NUM_CORES];
    int run_interval;           //length of 1 run interval in microseconds
} manager_state;


void manager_print(manager_state *manager){
    int i, j;
    printf("system budget: %f\n", manager->power_budget);
    for(i = 0; i < NUM_CORES; i++){
        printf("CORE %d\n", i);
        printf("    pstate: %d\n", manager->cores[i].pstate_index);
        printf("    mem bound: %d\n", manager->cores[i].mem_bound);
        printf("    estimated power: ");
        for(j = 0; j < AVAILABLE_PSTATES; j++){
            printf("%f ", manager->cores[i].estimated_power[j]);
        }
        printf("\n");
    }
}


void manager_init(manager_state *manager, float budget, int interval, int starting_pstate, power_state *pstates){
    int i;
    manager->power_budget = budget;
    manager->run_interval = interval;
    for(i = 0; i < NUM_CORES; i++){
        manager->cores[i].pstate_index = starting_pstate;
        manager->sorted_list[i] = i;
    }
    for(i = 0; i < AVAILABLE_PSTATES; i++){
        manager->available_pstates[i].freq = pstates[i].freq;
        manager->available_pstates[i].volt = pstates[i].volt;
    }
}

int manager_update_power(manager_state *manager, int core, float power){
    //update core power with actual power consumed in previous interval
    manager->cores[core].actual_power = power;
}

int manager_sort_mem_bound(manager_state *manager){
    //fill in the sorted_list array with the indecies of the cores from most to least memory bound
    int i, j;
    int temp;
    for(i = 0; i < NUM_CORES; i++){
        for(j = NUM_CORES - 1; j > i; j--){
            if(manager->cores[manager->sorted_list[j]].mem_bound > manager->cores[manager->sorted_list[j-1]].mem_bound){
                temp = manager->sorted_list[j];
                manager->sorted_list[j] = manager->sorted_list[j-1];
                manager->sorted_list[j-1] = temp;
            }            
        }
    }
    for(i = 0; i < NUM_CORES; i++){
        printf("%d\n",manager->sorted_list[i]);
    }
}

void manager_estimate_power(manager_state *manager){
    //fill in the projected power consumption for each core in each potential power state
    int i,j;
    float actual_power_norm, estimated_power;
    int freq;
    float volt;
    //update each core's projected power 
    for(i = 0; i < NUM_CORES; i++){
        //calcualte the normalized power
        actual_power_norm = manager->cores[i].actual_power;
        freq = manager->available_pstates[manager->cores[i].pstate_index].freq;
        volt = manager->available_pstates[manager->cores[i].pstate_index].volt;
        actual_power_norm = actual_power_norm/(freq*volt*volt);
        //fill in the core's table with projected power change
        for(j = 0; j < AVAILABLE_PSTATES; j++){
            freq = manager->available_pstates[j].freq;
            volt = manager->available_pstates[j].volt;
            estimated_power = actual_power_norm*freq*volt*volt;
            //fill in an entry in the estimate table with the estimated  power 
            manager->cores[i].estimated_power[j] = estimated_power;
        }
    }
}

bool set_membound(manager_state *manager, int core, int mb){
    manager->cores[core].mem_bound = mb;
}

bool is_membound(manager_state *manager, int core){
    if(manager->cores[core].mem_bound > MEMBOUND_THRESH){
        return(true);
    } else {
        return(false);
    }
}

void manager_assign_power(manager_state *manager, float power){
    //set the next power states assuming that we can allocate additional power
    int i;
    int core_num;
    int current_pstate;
    float additional_power = power;
    float current_power, new_power;
    for(i = NUM_CORES - 1; i >= 0; i--){
        core_num = manager->sorted_list[i];
        if(!is_membound(manager, core_num)){ //check if core could make use of extra power
           current_pstate = manager->cores[core_num].pstate_index;
           if(current_pstate != AVAILABLE_PSTATES -1){
              //There is room to raise this core's pstate
              current_power = manager->cores[core_num].estimated_power[current_pstate];
              new_power = manager->cores[core_num].estimated_power[current_pstate + 1];
              printf("new power: %f current_power %f additional_power %f\n", new_power, current_power, additional_power);
              if((new_power - current_power) <= additional_power){
                  //raising this core's power still fits in the budget
                  manager->cores[core_num].pstate_index++;
                  additional_power -= (new_power - current_power);
              }
           }
        } else {
            //no more cores can make use of extra power
            break;
        }
    }
}

void manager_reclaim_power(manager_state *manager, float power){
    //set the next power states assuming that we must reclaim some power
    int i;
    int core_num;
    int current_pstate;
    int first_non_membound_core;
    float reclaim_power = power;
    float current_power, new_power;
    bool done_search;
    //Step 1: Search the memory bound cores and drop power when possible
    //split the search space into mem bound and non-mem bound cores
    for(i = 0; i < NUM_CORES; i++){
       if(!is_membound(manager, manager->sorted_list[i])){
           break;
       }
    }
    //iterate over the mem bound cores until all are at pstate 0 or budget met
    first_non_membound_core = i;
    done_search = false;
    while((reclaim_power > 0) && !done_search) {
        done_search = true;
        for(i = 0; i < first_non_membound_core; i++){
            core_num = manager->sorted_list[i];
            current_pstate = manager->cores[core_num].pstate_index;
            if((current_pstate != 0) && (reclaim_power > 0)){
                  //There is room to lower this core's pstate
                  current_power = manager->cores[core_num].estimated_power[current_pstate];
                  new_power = manager->cores[core_num].estimated_power[current_pstate - 1];
                  manager->cores[core_num].pstate_index--;
                  reclaim_power -= (current_power - new_power);
                  done_search = false; 
            }
        }
    }
    printf("slack after search 1 = %f\n", reclaim_power);
    //check if there is any need to drop power for non-membound cores
    done_search = false;
    while((reclaim_power > 0) && !done_search) {
        done_search = true;
        for(i = first_non_membound_core; i < NUM_CORES; i++){
            core_num = manager->sorted_list[i];
            current_pstate = manager->cores[core_num].pstate_index;
            if((current_pstate != 0) && (reclaim_power > 0)){
                  //There is room to lower this core's pstate
                  current_power = manager->cores[core_num].estimated_power[current_pstate];
                  new_power = manager->cores[core_num].estimated_power[current_pstate - 1];
                  manager->cores[core_num].pstate_index--;
                  reclaim_power -= (current_power - new_power);
                  done_search = false; 
            }
        }
    }

}

void manager_evaluate(manager_state *manager){
    //find next pstate for each core
    int i;
    float total_power = 0;
    float slack;

    //find total actual power consumed in the previous interval
    for(i = 0; i < NUM_CORES; i++){
        total_power += manager->cores[i].actual_power;
    }
    slack = manager->power_budget - total_power; //negative number means power must be reclaimed
                                                //positive number means power can be allocated
    printf("Total power: %f\n", total_power);
    printf("slack: %f\n", slack);
    manager_sort_mem_bound(manager); 
    manager_estimate_power(manager);
 
    if(slack < 0) {
        printf("reclaming...\n");
        manager_reclaim_power(manager, slack*-1);
    } else {
        printf("allocating...\n");
        manager_assign_power(manager, slack);
    }   
}

int get_run_cycles(manager_state *manager, int core){
    //figure out how many cycles the next interval will execute
    int freq, cycles;
    freq = manager->available_pstates[manager->cores[core].pstate_index].freq;
    cycles = freq * (manager->run_interval); //(MHz)*(microseconds) = cycles
    return cycles;
}

int get_freq(manager_state *manager, int core){
    return(manager->available_pstates[manager->cores[core].pstate_index].freq);
}
